const spades = '♠'
const hearts = '♡'
const diamonds = '♢'
const clubs = '♣'
const suits = [hearts, spades, diamonds, clubs]
const numbers = ['A', 2, 3, 4, 5, 6, 7, 8, 9, 10, 'J', 'Q', 'K']
const cards = suits.flatMap(suit => numbers.map(number => number + suit))

cards.forEach(card => createNode(card))

function createNode(card) {
  const main = document.querySelector('#cards')

  const suit = card.slice(-1)

  const cardDiv = document.createElement('div');
  cardDiv.classList.add('card');
  const content = document.createTextNode(card);
  cardDiv.appendChild(content);
  cardDiv.classList.add('singlecard');
  cardDiv.classList.add(suit);

  main.appendChild(cardDiv);
}

